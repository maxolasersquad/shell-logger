# Shell Logger

A simple shell script to bring simple logging to shell scripting similar to the [RFC 5424](https://tools.ietf.org/html/rfc5424) logging spec. The intention is not to be RFC 5424 compatible, but function in a way that is familiar to anyone who has used a logging library that implements this spec.

## Usage

Seven logging levels are supported; DEBUG, INFORMATION, WARNING, ERROR, CRITICAL, ALERT, EMERGENCY. Details on these levels can be found in the [RFC 5424 spec](https://tools.ietf.org/html/rfc5424#page-11).

Before the logger can run it must be initialized with the log location. The file will be created if it does not exist, but the directory must already exist and be writable by the user running the script.

```sh
#!/bin/bash

source logger.sh
INITIALIZE ~/logs/test.log

DEBUG "Beginning to run test script."

echo Hello World!

DEBUG "Finished running the test script."
```

This will create a log that looks like this.

```
[test.sh] [Fri Oct  4 16:11:17 UTC 2019] [DEBUG]: Beginning to run test script.
[test.sh] [Fri Oct  4 16:11:17 UTC 2019] [DEBUG]: Finished running the test script.
```

You can use any custom log-level you want by calling LOG with your custom level as the first argument.

```sh
LOG LUDICROUS "My brains are going into my feet!"
```

### Custom Date Formats

A second argument can be passed to `INITIALIZE` to specify the date format. This can either be the values ["ISO8601"](https://en.wikipedia.org/wiki/ISO_8601), ["RFC3339"](https://tools.ietf.org/html/rfc3339), or your own formatter using the formatting rules specified by the GNU date documentation which you can get from the [website](http://www.gnu.org/software/coreutils/manual/html_node/date-invocation.html) or by running `man date` on your machine.

Technically RFC 3339 is the correct date format for an RFC 5424 logger. However, the specification for RFC 3339 provides some variability in formatting, while RFC 5424 specifies a precise version of RFC 3339 be implemented. The GNU date command does not use that version of RFC 3339, and such specifying this value will not give you the date format required by RFC 5424. The work around to this is to specify your own custom date format that correctly implements RFC 3339 in the format required by RFC 5424.

By default the date will be formatted with no specification sent to the `date` command, which formats based on the LC_TIME locale of your machine.

```sh
#!/bin/bash

source logger.sh
INITIALIZE ~/logs/test.log "ISO8601"

DEBUG "Beginning to run test script."

echo Hello World!

DEBUG "Finished running the test script."
```

This will create a log that looks like this.

```
[test.sh] [2019-10-04T16:07:17-04:00] [DEBUG]: Beginning to run test script.
[test.sh] [2019-10-04T16:07:17-04:00] [DEBUG]: Finished running the test script.
```
