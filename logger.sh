#!/bin/bash

BASE_NAME=`basename "$0"`
DATE_FORMAT=
SCRIPT_LOG=

function LOG() {
  if [ -z "$DATE_FORMAT" ]; then
    timeAndDate=`date`
  elif [ "$DATE_FORMAT" = "ISO8601" ]; then
    timeAndDate=`date --iso-8601=seconds`
  elif [ "$DATE_FORMAT" = "RFC3339" ]; then
    timeAndDate=`date --rfc-3339=seconds`
  else
    timeAndDate=`date $timeAndDate`
  fi
  level=$1
  message="$2"
  echo "[$BASE_NAME] [$timeAndDate] [$level]: $message" >> $SCRIPT_LOG
}

function DEBUG() {
  LOG DEBUG "$1"
}

function INFORMATION() {
  LOG INFORMATION "$1"
}

function NOTICE() {
  LOG NOTICE "$1"
}

function WARNING() {
  LOG WARNING "$1"
}

function ERROR() {
  LOG ERROR "$1"
}

function CRITICAL() {
  LOG CRITICAL "$1"
}

function ALERT() {
  LOG ALERT "$1"
}

function EMERGENCY() {
  LOG EMERGENCY "$1"
}

function INITIALIZE() {
  SCRIPT_LOG="$1"
  if [ ! -z "$2" ]; then
    DATE_FORMAT="$2"
  fi
}
